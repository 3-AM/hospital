$(document).ready(function(){
	$('input,textarea').focus(function(){
      $(this).data('placeholder',$(this).attr('placeholder'))
      $(this).attr('placeholder','');
    });
    $('input,textarea').blur(function(){
      $(this).attr('placeholder',$(this).data('placeholder'));
    });
	$('.header_container .navbar-header').find('button').click(function(){
		$('.header_container .nav_container').toggleClass('navbar_open');
	})
	$('.question').find('a.toggle_text').click(function(e){
		$(this).parent().find('span').toggleClass('hidden');
	    $(this).toggleClass('open');
		e.preventDefault()
	})
	$('.news_page div.news:first').addClass('first');
	$('blockquote').prepend('«');
	$('blockquote').append('»');
	$('.administration .text').find('button.btn_gray').click(function(){
		$(this).parent().next().toggle();
	})
	$('.administration .btn_gray').click(function(){
	    var hideBlock = $(this).parent().next();
	    $(this).text(hideBlock.is(':visible') ? 'Скрыть' : 'Подробнее');
	});
	$('.navbar-nav li').hover(function(){
		$(this).find('.subnav_container').fadeToggle(300);
	})
	$('.directory').find('a').click(function(e){
		e.preventDefault();
	})
	$('.directory').find('a.show_items').click(function(){
		$(this).parent().find('.hidden_items').slideToggle(200);
		$(this).toggleClass('toggle');
	})
	$('.directory a.show_items').click(function(){
	    var hideItems = $(this).parent().find('.hidden_items');
	    $(this).text(hideItems.is(':visible') ? 'свернуть' : 'еще');
	});
	if ($(window).width() < 767) {
			$('.directory').find('h2.letter').addClass('mobile_directory');
			$('.directory').find('p').hide();
		};
		if ($(window).width() > 767) {
			$('.directory').find('h2.letter').removeClass('mobile_directory');
			$('.directory').find('p').show();
		}
		$('.directory').find('.mobile_directory').click(function(){
		  $(this).parent().find('p').slideToggle(200);
		  $(this).parent().find('.hidden_items').slideToggle(200);
		  $(this).toggleClass('active');
	});
})
	
$(window).resize(function(){
		if ($(window).width() < 767) {
			$('.directory').find('h2.letter').addClass('mobile_directory');
			$('.directory').find('p').hide();
		};
		if ($(window).width() > 767) {
			$('.directory').find('h2.letter').removeClass('mobile_directory')
			$('.directory').find('p').show();
		}
	})